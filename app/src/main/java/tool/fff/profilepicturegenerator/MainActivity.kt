/*
 *     This file is part of the program "Fridays for Future Profilbildgenerator".
 *
 *     Copyright (C) 2019 Hocuri
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package tool.fff.profilepicturegenerator

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.StreamCorruptedException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.reflect.KClass


val dateOfNextDemo = GregorianCalendar(2020, Calendar.JANUARY, 17)
const val generatorUrl = "https://fridaysforfuture.de/dieuhrtickt/profilbild/"
const val infoUrl = "https://fridaysforfuture.de/dieuhrtickt/"
const val extraHint =
	"Hinweis: Das ist KEINER der globalen Streiks, bei denen Du wahrscheinlich warst, sondern ALLE treffen sich in Mainz. \n\n Wenn Du keine Lust hast, nach Mainz zu kommen, warte einfach bis zum nächsten globalen Streik.\n\nMehr Info auf https://fridaysforfuture.de/dieuhrtickt/."
const val extraTitle = " (Demo in Mainz)"
// Remember to change the app icon!


var renderedBitMap: Bitmap? = null
var renderedBitMapUri: Uri? = null

class MainActivity : AppCompatActivity() {

	private var currentDialog: Dialog? = null
	private var mUploadMessage: ValueCallback<Array<Uri>>? = null
	private val FILECHOOSER_RESULTCODE = 1
	private val handler = Handler()

	@SuppressLint("ObsoleteSdkInt")
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)


		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && BuildConfig.DEBUG) {
			// Uncomment to test the ShareRenderedPictureActivity:
			//renderedBitMap = getDrawable(R.mipmap.ic_launcher)!!.toBitmap(200, 200)
		}

		if (renderedBitMap != null) {
			switchTo(ShareRenderedPictureActivity::class)
			return
		}

		setContentView(R.layout.activity_main)
		setSupportActionBar(toolbar)

		@Suppress("ConstantConditionIf")
		if (dateOfNextDemo < GregorianCalendar()) {
			text_title.text =
				"Die Demo vom ${SimpleDateFormat.getDateInstance().format(dateOfNextDemo.time)} ist vorbei. Danke an alle, die gekommen sind!\n\n" +
						"Ca. einen Monat vor der nächsten Demo gibt es ein Update für diese App, " +
						"sodass sie das neue Profilbild generiert. Bis dahin kannst du dich auf fridaysforfuture.de " +
						"über Neuigkeiten informieren.\n\nDu kannst diese App trotzdem ausprobieren, aber es kann sein, dass sie nicht mehr richtig funktioniert."
			text_title.setTextColor(Color.RED)
		} else if (extraTitle != "") {
			text_title.text = text_title.text.toString() + extraTitle
		}

		@Suppress("ConstantConditionIf")
		if (extraHint != "") {
			extra_hint.visibility = View.VISIBLE
			extra_hint.text = extraHint
		}

		button_generate.setOnClickListener {
			start()
			// The webview can't be used a second time (I did not find out why).
			// So, disable the button to prevent an accidental second click (we will recreate()
			// or finish() this activity anyway)
			button_generate.isEnabled = false
		}

	}

	override fun onDestroy() {
		super.onDestroy()
		Log.i(TAG, "onDestroy")
		handler.removeCallbacksAndMessages(null)
		currentDialog?.dismiss()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return when (item.itemId) {
			R.id.action_about -> {
				switchTo(AboutActivity::class)
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}


	// ============================================================================================================================
	// First, load the website and try to click the "Generate" button once a second
	// (until it is loaded and the button can be clicked)
	// ============================================================================================================================
	@SuppressLint("SetJavaScriptEnabled")
	fun start() {
		setErrorListener(webView)
		webView.settings.javaScriptEnabled = true
		webView.settings.allowFileAccess = true
		webView.loadUrl(generatorUrl)

		showProgressDialog("Profilbildgenerator wird geladen")


		webView.setDownloadListener(::downloadListener)


		fun tryToClickChooserButton() {
			clickButtonWithClass("btn-generate")
			handler.postDelayed(::tryToClickChooserButton, 1000)
		}

		tryToClickChooserButton()
	}


	@SuppressLint("AddJavascriptInterface")
	fun setErrorListener(webView: WebView) {

		webView.webViewClient = object : WebViewClient() {
			override fun onReceivedError(
				view: WebView,
				errorCode: Int, description: String, failingUrl: String
			) {
				when (errorCode) {
					ERROR_CONNECT, ERROR_HOST_LOOKUP, ERROR_TIMEOUT -> {
						Log.e(TAG, "Web view connection error $errorCode, $description")
						currentDialog?.dismiss()
						showErrorDialog(
							"Keine Internetverbindung",
							"Zum Generieren des Profilbilds wird eine Internetverbindung benötigt."
						)
					}
					else -> {
						Log.e(TAG, "Web view error $errorCode, $description")
						showErrorDialog(
							"Profilbildgenerator konnte nicht geladen werden    ",
							"Fehler $errorCode: $description"
						)
						currentDialog?.dismiss()

					}
				}
			}
		}


		// ============================================================================================================================
		// Secondly, when the "Generate  button was pressed, this function will be called
		// and ask the user to choose the current picture:
		// ============================================================================================================================
		webView.webChromeClient = object : WebChromeClient() {
			override fun onProgressChanged(view: WebView, progress: Int) {
			}

			override fun onShowFileChooser(
				webView: WebView,
				filePathCallback: ValueCallback<Array<Uri>>,
				fileChooserParams: FileChooserParams
			): Boolean {

				Log.i(TAG, "onShowFileChooser")
				handler.removeCallbacksAndMessages(null) // Stop trying to click button
				currentDialog?.dismiss()

				this@MainActivity.mUploadMessage = filePathCallback
				val i = Intent(Intent.ACTION_GET_CONTENT)
				i.addCategory(Intent.CATEGORY_OPENABLE)
				i.type = "image/*"
				startActivityForResult(
					Intent.createChooser(i, "File Chooser"),
					FILECHOOSER_RESULTCODE
				)

				return true
			}
		}

	}


	// ============================================================================================================================
	// This is called when the user chose a picture:
	// It will tell this to the website and click the "Download" button once a second
	// until the picture can be downloaded
	// ============================================================================================================================
	@Suppress("DEPRECATION")
	override fun onActivityResult(
		requestCode: Int, resultCode: Int,
		intent: Intent?
	) {
		super.onActivityResult(requestCode, resultCode, intent)
		if (requestCode == FILECHOOSER_RESULTCODE) {

			Log.i(TAG, "picture was chosen")

			if (null == mUploadMessage) return
			if (intent == null || resultCode != Activity.RESULT_OK || intent.data == null) {
				recreate()
				return
			}

			val result = arrayOf(intent.data!!)
			mUploadMessage!!.onReceiveValue(result)
			mUploadMessage = null


			Log.i(TAG, "tryToClickDownloadButton starting...")

			fun tryToClickDownloadButton() {
				clickButtonWithClass("btn-download")
				handler.postDelayed(::tryToClickDownloadButton, 1000)
			}

			tryToClickDownloadButton()

			showProgressDialog("Profilbild wird generiert, dauert ca. 20 Sekunden")

		}
	}

	fun clickButtonWithClass(className: String) {
		webView.loadUrl(
			"""javascript:(function(){
				        var genElements=document.getElementsByClassName('$className');
						if (genElements.length > 0) {
							console.log('clicking $className button');
							var HtmlEvent=document.createEvent('HTMLEvents');
							HtmlEvent.initEvent('click',true,true);
							genElements[0].dispatchEvent(HtmlEvent);
						} else {
							console.log('No $className button found');
						}
					})()"""
		)
	}

	// ============================================================================================================================
	// ...And here we download the picture.
	// ============================================================================================================================
	@Suppress("UNUSED_PARAMETER")
	private fun downloadListener(
		url: String,
		userAgent: String,
		contentDisposition: String,
		mimetype: String,
		contentLength: Long
	) {
		if (!url.startsWith("data:image/png;base64,")) return

		Log.i(TAG, "Download ${url.subSequence(0, 40)}...")

		val decodedString =
			Base64.decode(url.removePrefix("data:image/png;base64,"), Base64.DEFAULT)
		val bitMap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
			?: throw StreamCorruptedException("bitMap was null")
		renderedBitMap = bitMap
		renderedBitMapUri = null
		switchTo(ShareRenderedPictureActivity::class)
	}


	// ============================================================================================================================
	// Finally, the helper functions:
	// ============================================================================================================================

	private fun showProgressDialog(message: String) {
		val d = ProgressDialog(this)
		d.setTitle("Lädt...")
		d.setMessage(message)
		d.setCancelable(false)
		d.setButton(DialogInterface.BUTTON_NEGATIVE, "Abbrechen") { _, _ ->
			currentDialog = AlertDialog.Builder(this)
				.setMessage("Möchtest du wirklich abbrechen?")
				.setPositiveButton("Ja, abbrechen") { _, _ ->
					recreate()
				}
				.setNeutralButton("Nein, weiter") { _, _ ->
					showProgressDialog(message)
				}
				.setOnCancelListener {
					showProgressDialog(message)
				}
				.show()
		}
		d.show()
		currentDialog = d
	}

	private fun showErrorDialog(
		title: String,
		message: String
	) {
		AlertDialog.Builder(this)
			.setTitle(title)
			.setMessage(message)
			.setPositiveButton("OK") { _, _ -> recreate() }
			.setOnCancelListener { recreate() }
			.show()
	}

}

fun Activity.switchTo(activity: KClass<out Activity>) {
	startActivity(Intent(this, activity.java))
	finish()
}

fun Context.viewInBrowser(uri: String) {
	val intent = Intent(
		Intent.ACTION_VIEW,
		Uri.parse(uri)
	)
	if (intent.resolveActivity(packageManager) != null)
		startActivity(intent)
	else
		Toast.makeText(this, "No browser found. Maybe you should install the app 'Firefox'?", Toast.LENGTH_LONG).show()
}



const val TAG = "FfF-profile-code"