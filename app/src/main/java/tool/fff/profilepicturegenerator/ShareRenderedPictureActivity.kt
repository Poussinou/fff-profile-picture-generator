/*
 *     This file is part of the program "Fridays for Future Profilbildgenerator".
 *
 *     Copyright (C) 2019 Hocuri
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package tool.fff.profilepicturegenerator

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import kotlinx.android.synthetic.main.activity_share_rendered_picture.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream

class ShareRenderedPictureActivity : AppCompatActivity() {

	private val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 358
	private var active = false

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_share_rendered_picture)
		active = true

		if (renderedBitMap == null) {
			renderedBitMapUri = null
			switchTo(MainActivity::class)
			return
		}

		imageView.setImageBitmap(renderedBitMap)

		button_done.setOnClickListener {
			renderedBitMap = null
			renderedBitMapUri = null
			active = false
			switchTo(SupportActivity::class)
		}

		trySavingFile()
	}

	private fun trySavingFile() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
			!= PackageManager.PERMISSION_GRANTED
		) {
			// Permission is not granted
			if (ActivityCompat.shouldShowRequestPermissionRationale(
					this,
					Manifest.permission.WRITE_EXTERNAL_STORAGE
				)
			) {
				AlertDialog.Builder(this)
					.setTitle("Berechtigung benötigt")
					.setMessage("Bitte klicke gleich auf \"Zulassen\", damit das Profilbild gespeichert werden kann.")
					.setPositiveButton("OK") { _, _ ->
						requestPermission()
					}
					.setCancelable(false)
					.show()
			} else {
				requestPermission()
			}
		} else {
			// Permission has already been granted

			if (renderedBitMapUri == null) {
				val uri = saveBitmapLegacy(renderedBitMap!!)?.toUri()
					?: saveBitmapMediaStore(renderedBitMap!!, "FfFProfilbild.jpeg")
				Log.i(TAG, "Saved image to $uri")
				renderedBitMapUri = uri
			}

			if (renderedBitMapUri != null) {
				textViewSaved.text = "Bild gespeichert; antippen, um es in der Gallerie anzuzeigen"
				setupListeners(renderedBitMapUri!!)
			}

		}

	}

	@SuppressLint("InlinedApi")
	private fun requestPermission() {
		ActivityCompat.requestPermissions(
			this,
			arrayOf(
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.READ_EXTERNAL_STORAGE
			),
			MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
		)
	}

	private fun setupListeners(file: Uri) {

		if (Build.VERSION.SDK_INT >= 24) {
			try {
				val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
				m.invoke(null)
			} catch (e: Exception) {
				e.printStackTrace()
			}
		}

		button_whatsapp.isEnabled = true
		button_whatsapp.setOnClickListener {
			val intent = Intent(Intent.ACTION_ATTACH_DATA)
			intent.setDataAndType(file, "image/jpg")
			intent.putExtra("mimeType", "image/jpg")
			startActivityForResult(Intent.createChooser(intent, "Bild benutzen als"), 200)
		}

		fun openExternal(v: View) {
			Log.i(TAG, "Opening with other application")
			val intent = Intent(Intent.ACTION_VIEW)
			intent.setDataAndType(file, "image/jpg")
			intent.putExtra("mimeType", "image/jpg")
			startActivity(Intent.createChooser(intent, "Öffnen mit"))
		}

		imageView.setOnClickListener(::openExternal)
		textViewSaved.setOnClickListener(::openExternal)

	}


	private fun saveBitmapLegacy(bitmap: Bitmap): File? {

		try {

			val picturesDirectory =
				Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)

			if (!picturesDirectory.exists()) {
				picturesDirectory.mkdirs()
			}

			var file = File(picturesDirectory, "FridaysForFuture-Profilbild.jpg")

			var i = 0
			while (file.exists()) {
				file = File(picturesDirectory, "FridaysForFuture-Profilbild-$i.jpg")
				i++
			}
			file.createNewFile()

			val out = FileOutputStream(file)
			bitmap.compress(Bitmap.CompressFormat.JPEG, 95, out)

			out.flush()
			out.close()
			return file

		} catch (e: Exception) {
			e.printStackTrace()
			Log.e(TAG, "Failed to save file legacy way: $e")
			return null
		}
	}

	@Throws(IOException::class)
	private fun saveBitmapMediaStore(
		bitmap: Bitmap,
		displayName: String
	): Uri? {
		val relativeLocation = Environment.DIRECTORY_PICTURES

		val contentValues = ContentValues()
		contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, displayName)

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
			contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, relativeLocation)

		val resolver = contentResolver

		var stream: OutputStream? = null
		var uri: Uri? = null

		try {
			val contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
			uri = resolver.insert(contentUri, contentValues)
				?: throw IOException("Failed to create new MediaStore record.")

			stream = resolver.openOutputStream(uri)
				?: throw IOException("Failed to get output stream.")

			if (bitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream) == false) {
				throw IOException("Failed to save bitmap.")
			}
			return uri
		} catch (e: IOException) {
			if (uri != null) {
				resolver.delete(uri, null, null)
			}
			e.printStackTrace()
			Log.e(TAG, "Failed to save using MediaStore: $e")
			return null
		} finally {
			stream?.close()
		}
	}

	override fun onRequestPermissionsResult(
		requestCode: Int,
		permissions: Array<String>, grantResults: IntArray
	) {
		when (requestCode) {
			MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
				if (active) {
					trySavingFile()
				} else {
					Log.e(TAG, "ShareRenderedPictureActivity was already left by user but onRequestPermissionsResult called")
				}
			}
			else -> {
				// Ignore all other requests.
			}
		}
	}


	override fun onBackPressed() {
		renderedBitMap = null
		renderedBitMapUri = null
		active = false
		switchTo(MainActivity::class)
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		if (item.itemId == android.R.id.home) {
			renderedBitMap = null
			renderedBitMapUri = null
			active = false
			switchTo(MainActivity::class)
		}
		return true
	}
}
