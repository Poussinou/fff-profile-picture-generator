/*
 *     This file is part of the program "Fridays for Future Profilbildgenerator".
 *
 *     Copyright (C) 2019 Hocuri
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package tool.fff.profilepicturegenerator

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_support.*

class SupportActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_support)

		button_donate.setOnClickListener {
			viewInBrowser("https://fridaysforfuture.de/spenden/")
		}

		button_done_support.setOnClickListener {
			switchTo(MainActivity::class)
		}

		button_feedback.setOnClickListener {
			val emailIntent = Intent(
				Intent.ACTION_SENDTO, Uri.fromParts(
					"mailto", "fff-profilbild-generator@web.de", null
				)
			)
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
			intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("fff-profilbild-generator@web.de"))
			startActivity(Intent.createChooser(emailIntent, "Senden über..."))
		}

		button_stars.setOnClickListener {
			try {
				startActivity(
					Intent(
						Intent.ACTION_VIEW,
						Uri.parse("market://details?id=$packageName")
					).apply {
						addFlags(
							Intent.FLAG_ACTIVITY_NO_HISTORY or
									Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
									Intent.FLAG_ACTIVITY_MULTIPLE_TASK
						)
					}
				)
			} catch (e: ActivityNotFoundException) {
				viewInBrowser("http://play.google.com/store/apps/details?id=$packageName")
			}
		}

		textViewComeAll.text = getString(R.string.und_das_wichtigste_kommt_mit_m_glichst_vielen_freunden_zur_demo) + " " + infoUrl
	}

	override fun onBackPressed() {
		switchTo(MainActivity::class)
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		if (item.itemId == android.R.id.home) {
			switchTo(MainActivity::class)
		}
		return true
	}
}
